package model.logic;


public class DatoPunto5B 
{
	private long idPelicula;
	private double rating;
	
	
	public long darIDPelicula()
	{
		return idPelicula;
	}
	
	public void setIDPelicula(long pIDPelicula)
	{
		this.idPelicula = pIDPelicula;
	}
	public double darRating(){
		return rating;
	}
	
	
	public void setRating(double pRating){
		this.rating = pRating;
	}

}

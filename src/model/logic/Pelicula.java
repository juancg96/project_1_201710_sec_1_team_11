package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class Pelicula {
	
	private long id;
	private String nombre;
	private int agno;
	private ILista<DatoPelicula> ratings;
	private ILista<DatoPelicula> tags;
	private ILista<String> generos;
	
	public Pelicula(long pId, String pNombre, int pAgno){
		id = pId;
		nombre = pNombre;
		agno = pAgno;
		ratings = new ListaEncadenada<DatoPelicula>();
		tags = new ListaEncadenada<DatoPelicula>();
		generos = new ListaEncadenada <String>(); //el primer genero de la lista es el genero principal
		
	}
	
	public long darId(){
		return id;
	}
	public String darNombre(){
		return nombre;
	}
	public int darAgno(){
		return agno;
	}
	public ILista<DatoPelicula> darRatings(){
		return ratings;
	}
	public ILista<DatoPelicula> darTags(){
		return tags;
	}
	public ILista<String> darGeneros(){
		return generos;
	}
	public void agregarGenero(String p){
		generos.agregarElementoFinal(p);
	}
	public void agregarRating(DatoPelicula p){
		ratings.agregarElementoFinal(p);
	}
	public void agregarTag(DatoPelicula p){
		tags.agregarElementoFinal(p);
	}
	public double darPromedioRating(){
		double sumatoria = 0;
		for(int i = 0; i<ratings.size(); i++){
			sumatoria += Double.parseDouble(ratings.get(i).darData());
		}
		return sumatoria / ratings.size();
	}
}

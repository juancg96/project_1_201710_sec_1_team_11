package model.logic;

import model.data_structures.ILista;
import model.vo.VOPelicula;
import model.vo.VOTag;
import model.vo.VOUsuario;


public class Genero {
	
	private String nombre;
	private ILista<Pelicula> peliculas;
	private ILista<VOUsuario> usuarios;
	private ILista<VOTag> tags;
	private ILista<VOPelicula> vopeliculas;
	
	
	public String darNombre()
	{
		return nombre;
	}
	public ILista<Pelicula> darPeliculas(){
		return peliculas;
	}
	
	public ILista<VOUsuario> darUsuarios()
	{
		return usuarios;
	}
	
	public ILista<VOTag> darTags()
	{
		return tags;
	}
	
	public ILista<VOPelicula> darVOPeliculas()
	{
		return vopeliculas;
	}
	
	
	public void setVOPeliculas(ILista<VOPelicula> pVOPeliculas)
	{
		this.vopeliculas = pVOPeliculas;
	}
	
	public void setUsuarios(ILista<VOUsuario> pUsuarios)
	{
		this.usuarios = pUsuarios;
	}
	
	
	public void setNombre(String pNombre){
		this.nombre = pNombre;
	}
	
	public void setPeliculas(ILista<Pelicula> pPeliculas)
	{
		this.peliculas = pPeliculas;
	}
	
	public void setTags(ILista<VOTag> pTags)
	{
		this.tags = pTags;
	}
	
	
}

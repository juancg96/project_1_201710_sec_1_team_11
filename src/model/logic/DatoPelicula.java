package model.logic;

public class DatoPelicula {

	private int userID;
	private int movieID;
	private String data;
	private long timestamp;
	
	public DatoPelicula(int pUser, int pMovie, String pData, long l){
		userID = pUser;
		movieID = pMovie;
		data = pData;
		timestamp = l;
	}
	
	public int darUserID(){
		return userID;
	}
	public int darmovieID(){
		return movieID;
	}
	public String darData(){
		return data;
	}
	public long darTimeStamp(){
		return timestamp;
	}
	
}

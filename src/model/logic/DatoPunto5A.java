package model.logic;

import model.vo.VOPelicula;

public class DatoPunto5A {
	
	private VOPelicula pelicula;
	private double calificacion;
	private double diferencia;
	
	public VOPelicula darPelicula(){
		return pelicula;
	}
	public double darCalificacion(){
		return calificacion;
	}
	public double darDiferencia(){
		return diferencia;
	}
	public void setPelicula(VOPelicula pPelicula){
		this.pelicula = pPelicula;
	}
	public void setCalificacion(double pCalificacion){
		this.calificacion = pCalificacion;
	}
	public void setDiferencia(double pDiferencia){
		this.diferencia = pDiferencia;
	}
}

package model.data_structures;

public class Cola<T> {
	//Queue

	private ListaEncadenada<T> queue;

	public Cola(){
		queue = new ListaEncadenada<T>();
	}

	public void enqueue(T elem){
		queue.agregarElementoFinal(elem);
	}
	public T dequeue(){
		T data = queue.get(0);
		queue.eliminarElemento(0);
		return data;
	}
	public int size(){
		return queue.size();
	}
	public boolean isEmpty(){
		if(queue.size() == 0){
			return true;
		}
		else{
			return false;
		}
	}

}


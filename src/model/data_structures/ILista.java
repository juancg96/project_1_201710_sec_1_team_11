package model.data_structures;

import model.vo.VOUsuario;

public interface ILista<T> extends Iterable<T>{

	public void agregarElementoFinal(T elem);
	
	public T get(int pos);
	
	public T eliminarElemento(int pos);
	
	public int size();
	
	public void exchange(int a, int b);
	
	public void set(int a, T data);

}

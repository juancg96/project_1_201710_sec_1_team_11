package model.data_structures;

import java.util.Iterator;

import model.vo.VOUsuario;

public class ListaEncadenada<T> implements ILista<T>{

	private int cantidadNodos;
	private Nodo<T> primerNodo;
	private Nodo<T> ultimoNodo;

	public  ListaEncadenada(){
		cantidadNodos = 0;
		primerNodo = null;
		ultimoNodo = null;
	}

	public void agregarElementoFinal(T elem) {
		//Revisa si no hay nodos
		if(cantidadNodos==0){
			Nodo<T> p= new Nodo<T>(elem);
			primerNodo = p;
			ultimoNodo = p;
		}
		//Si hay nodos, lo agrega despues del ultimo.
		else{
			Nodo<T> p=  new Nodo<T>(elem);
			p.asignarAnterior(ultimoNodo);
			ultimoNodo.asignarSiguiente(p);
			ultimoNodo = p;
		}
		cantidadNodos++;
	}



	public T get(int pos) {
		{
			Nodo<T> nodoActual = primerNodo;
			int recorrido = 0;
			while(nodoActual.darSiguiente() != null && recorrido < pos ){
				nodoActual = nodoActual.darSiguiente();
				recorrido++;
			}
			return nodoActual.darData();
		}
	}

	public int size() {
		return cantidadNodos;
	}

	public T eliminarElemento(int pos){
		T r = null;
		//si es el primer nodo
		if(pos == 0){
			r = primerNodo.darData();
			primerNodo = primerNodo.darSiguiente();
		}
		//si es el ultimo nodo
		else if(pos == cantidadNodos-1){
			r = ultimoNodo.darData();
			ultimoNodo = ultimoNodo.darAnterior();
		}
		else{
			int recorrido = 0;
			Nodo<T> actual = primerNodo;
			while(actual.darSiguiente() != null && recorrido < pos){
				actual = actual.darSiguiente();
				recorrido++;
			}
			r = actual.darData();
			actual.darSiguiente().asignarAnterior(actual.darAnterior());
			actual.darAnterior().asignarSiguiente(actual.darSiguiente());
		}
		cantidadNodos--;
		return r;
	}


	public void exchange(int a, int b){
		T dataA = get(a);
		T dataB = get(b);
		Nodo <T> actual = primerNodo;
		int posiciones = 0;
		//nodoA
		while(posiciones != a){
			actual = actual.darSiguiente();
			posiciones++;
		}
		actual.cambiarData(dataB);

		actual = primerNodo;
		posiciones = 0;
		//nodoB
		while(posiciones != b){
			actual = actual.darSiguiente();
			posiciones++;
		}
		actual.cambiarData(dataA);
	}

	
	public void set(int a, T data){
		Nodo<T> actual = primerNodo;
		int posiciones = 0;
		while(posiciones < a){
			actual = actual.darSiguiente();
			posiciones++;
		}
		actual.cambiarData(data);
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
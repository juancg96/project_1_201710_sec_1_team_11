package model.data_structures;

public class Pila<T> {
	//Stack
	
	private Nodo<T> tope;
	private int cantidad;

	public Pila(){
		tope = null;
		cantidad = 0;
	}

	public T pop() {
		if (tope != null) {
			T elem = tope.darData();
			tope = tope.darSiguiente();
			cantidad--;
			return elem;	
		}
		else{ return null;
		}
	}

	public void push(T elem) {
		Nodo<T> t = new Nodo<T>(elem);
		t.asignarSiguiente(tope);
		tope = t;
		cantidad++;
	}

	public int size(){
		return cantidad;
	}
	
	public boolean isEmpty(){
		if(tope == null){
			return true;
		}
		else{
			return false;
		}
	}
	public T darTope(){
		return tope.darData();
	}
}
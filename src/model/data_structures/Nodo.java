package model.data_structures;

public class Nodo<T> {

	private T data;
	private Nodo<T> siguiente;
	private Nodo<T> anterior;

	public Nodo(T pData){
		data = pData;
		siguiente = null;
		anterior = null;
	}

	public void asignarSiguiente(Nodo<T> pNodo)
	{
		siguiente = pNodo;
	}

	public void asignarAnterior(Nodo<T> pNodo)
	{
		anterior= pNodo;
	}

	public boolean tieneSiguiente()
	{
		boolean tiene = true;
		if(siguiente == null){
			tiene = false;
		}
		return tiene;
	}

	public boolean tieneAnterior()
	{
		boolean tiene = true;
		if(anterior == null)
		{
			tiene = false;
		}
		return tiene;
	}

	public Nodo<T> darSiguiente()
	{
		return siguiente;
	}

	public Nodo<T> darAnterior()
	{
		return anterior;
	}
	public T darData(){
		return data;
	}
	
	public void cambiarData(T pData){
		data = pData;
	}
	
}

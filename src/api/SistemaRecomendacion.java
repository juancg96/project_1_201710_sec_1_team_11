package api;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Instant;


import model.data_structures.Cola;
import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.Pila;
import model.logic.DatoPelicula;
import model.logic.DatoPunto5A;
import model.logic.DatoPunto5B;
import model.logic.Genero;
import model.logic.Pelicula;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;

public class SistemaRecomendacion implements ISistemaRecomendacionPeliculas{

	private ILista<Pelicula> peliculas;
	private ILista<String> generos;
	private ILista<DatoPelicula> ratings;
	private ILista<DatoPelicula> tags;

	private ILista<Genero> listaPrincipalGeneros;
	private Pila<VOOperacion> pilaAcciones;
	private ILista<VOUsuarioGenero> usuariosGenero;
	private ILista<VOGeneroUsuario> generoUsuario;
	private ILista<VORating> voratings;

	public void setUsuarioGenero(ILista<VOUsuarioGenero> pUsuarioGenero)
	{
		this.usuariosGenero = pUsuarioGenero;
	}


	public ILista<VOUsuarioGenero> darUsuariosGenero()
	{
		return usuariosGenero;
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		pilaAcciones = new Pila<VOOperacion>();
		peliculas = new ListaEncadenada<Pelicula>();
		generos = new ListaEncadenada<String>();
		boolean sePudo = true;
		BufferedReader br = null;
		try {
			String lineaActual = "";
			br = new BufferedReader(new FileReader(rutaPeliculas));
			lineaActual = br.readLine(); // se salta la primera linea ya que esta no tiene datos.
			while ((lineaActual = br.readLine()) != null) {
				int cuantasComas = 0;
				for(int  i = 0; i<lineaActual.length(); i++){
					if(lineaActual.charAt(i) == ','){
						cuantasComas++;	
					}
				}

				int id = Integer.parseInt(lineaActual.split(",")[0].trim());
				String[] textoSeparadoComa = lineaActual.split(",");
				String textoEntreIDyGeneros = "";
				for(int k = 1; k<cuantasComas; k++){
					textoEntreIDyGeneros = textoEntreIDyGeneros.trim() + " " + textoSeparadoComa[k].trim();;
				}

				int cuantosParentesis = 0;
				for(int h = 0; h<textoEntreIDyGeneros.length(); h++){
					if(textoEntreIDyGeneros.charAt(h) == '('){
						cuantosParentesis++;
					}
				}

				int agno = Integer.parseInt(textoEntreIDyGeneros.split("\\(")[cuantosParentesis].split("\\)")[0]);
				String[] textoNombre = textoEntreIDyGeneros.split("\\(");
				String nombre = "";
				for(int a = 0; a<cuantosParentesis; a++){
					nombre = nombre + textoNombre[a].trim();
				}
				String[] generosActual = lineaActual.split(",")[cuantasComas].split("\\|");

				Pelicula peliculaActual = new Pelicula(id, nombre, agno);
				peliculas.agregarElementoFinal(peliculaActual);
				for(int i = 0; i<generosActual.length; i++){
					generos.agregarElementoFinal(generosActual[i]);
				}

			}


		} catch (FileNotFoundException e) {
			sePudo = false;
			e.printStackTrace();
		} catch (IOException e) {
			sePudo = false;
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		estructurarDatos();
		return sePudo;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		ratings = new ListaEncadenada<DatoPelicula>();
		BufferedReader br = null;
		boolean sePudo = true;

		try {

			br = new BufferedReader(new FileReader(rutaRatings));
			String lineaActual = br.readLine(); //se salta la primera linea
			while ((lineaActual = br.readLine()) != null) {
				String[] datos = lineaActual.split(",");
				int userID = Integer.parseInt(datos[0]);
				int movieID = Integer.parseInt(datos[1]);
				String rating = datos[2];
				int timestamp = Integer.parseInt(datos[3]);
				DatoPelicula datosActual = new DatoPelicula(userID, movieID, rating, timestamp);
				ratings.agregarElementoFinal(datosActual);
			}

		} catch (FileNotFoundException e) {
			sePudo = false;
			e.printStackTrace();
		} catch (IOException e) {
			sePudo = false;
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		estructurarDatos();
		return sePudo;
	}



	@Override
	public boolean cargarTagsSR(String rutaTags) {
		tags= new ListaEncadenada<DatoPelicula>();
		BufferedReader br = null;
		boolean sePudo = true;

		try {

			br = new BufferedReader(new FileReader(rutaTags));
			String lineaActual = br.readLine(); //se salta la primera linea
			while ((lineaActual = br.readLine()) != null) {
				String[] datos = lineaActual.split(",");
				int userID = Integer.parseInt(datos[0]);
				int movieID = Integer.parseInt(datos[1]);
				String tag = datos[2];
				int timestamp = Integer.parseInt(datos[3]);
				DatoPelicula datosActual = new DatoPelicula(userID, movieID, tag, timestamp);
				tags.agregarElementoFinal(datosActual);

			}

		} catch (FileNotFoundException e) {
			sePudo = false;
			e.printStackTrace();
		} catch (IOException e) {
			sePudo = false;
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		estructurarDatos();
		return sePudo;
	}



	@Override
	public int sizeMoviesSR() {
		return peliculas.size();
	}

	@Override
	public int sizeUsersSR() {
		ILista<Integer> ratingsYTags = new ListaEncadenada<Integer>();
		for(int i = 0; i<ratings.size(); i++){
			ratingsYTags.agregarElementoFinal(ratings.get(i).darUserID());
		}
		for(int j = 0; j<tags.size(); j++){
			ratingsYTags.agregarElementoFinal(tags.get(j).darUserID());
		}

		int repetidos = 0;
		for(int i = 0; i<ratingsYTags.size(); i++){
			for(int j = 0; j<ratingsYTags.size(); j++){
				if(ratingsYTags.get(i) == ratingsYTags.get(j) && i != j){
					repetidos++;
				}
			}
		}
		return ratingsYTags.size() - repetidos;

	}

	@Override
	public int sizeTagsSR() {
		return tags.size();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		long inicio = Instant.now().getEpochSecond();

		ILista<VOGeneroPelicula> retorno = new ListaEncadenada<VOGeneroPelicula>();
		for(int i = 0; i<listaPrincipalGeneros.size(); i++){
			VOGeneroPelicula actualGeneroPelicula = new VOGeneroPelicula();
			actualGeneroPelicula.setGenero(listaPrincipalGeneros.get(i).darNombre());
			ILista<Pelicula> peliculasActual = listaPrincipalGeneros.get(i).darPeliculas();
			ILista<Pelicula> peliculasActualOrdenado = quickSortRatingsPeliculas(peliculasActual, 0, peliculasActual.size()-1);
			ILista<VOPelicula> voPeliculas = new ListaEncadenada<VOPelicula>();
			for(int j = peliculasActualOrdenado.size()-1; j<=peliculasActualOrdenado.size()-n; j--){
				voPeliculas.agregarElementoFinal(convertirVOPelicula(peliculasActualOrdenado.get(j)));
			}
			actualGeneroPelicula.setPeliculas(voPeliculas);
			retorno.agregarElementoFinal(actualGeneroPelicula);
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("peliculasPopularesSR", inicio, end);
		return retorno;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		long inicio = Instant.now().getEpochSecond();

		ILista<Pelicula> lista = mergesort(peliculas, 0, peliculas.size()-1);
		ILista<VOPelicula> retorno = new ListaEncadenada<VOPelicula>();
		for(int i = 0; i<peliculas.size(); i++){
			retorno.agregarElementoFinal(convertirVOPelicula(lista.get(i)));
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("catalogoPeliculasOrdenadoSR", inicio, end);
		return retorno;	


	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		long inicio = Instant.now().getEpochSecond();

		ILista<VOGeneroPelicula> retorno = new ListaEncadenada<VOGeneroPelicula>();
		for(int i = 0; i<listaPrincipalGeneros.size(); i++){
			VOGeneroPelicula generoActual = new VOGeneroPelicula();
			generoActual.setGenero(listaPrincipalGeneros.get(i).darNombre());

			double mayor = 0;
			Pelicula mejorPeli = listaPrincipalGeneros.get(i).darPeliculas().get(0);

			for(int j = 0; j<listaPrincipalGeneros.get(i).darPeliculas().size(); j++){
				Pelicula peliActual = listaPrincipalGeneros.get(i).darPeliculas().get(j);
				if(peliActual.darPromedioRating() > mayor){
					mayor = peliActual.darPromedioRating();
					mejorPeli = peliActual;
				}
			}
			VOPelicula peliculaConMejorRating = convertirVOPelicula(mejorPeli);
			ILista<VOPelicula> pelicula = new ListaEncadenada<VOPelicula>();
			pelicula.agregarElementoFinal(peliculaConMejorRating);
			generoActual.setPeliculas(pelicula);
			retorno.agregarElementoFinal(generoActual);
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("recomendarGeneroSR", inicio, end);
		return retorno;
	}



	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		long inicio = Instant.now().getEpochSecond();

		ILista<VOGeneroPelicula> retorno = new ListaEncadenada<VOGeneroPelicula>();
		for(int i = 0; i<listaPrincipalGeneros.size(); i++){
			VOGeneroPelicula generopelicula = new VOGeneroPelicula();
			generopelicula.setGenero(listaPrincipalGeneros.get(i).darNombre());
			ILista<VOPelicula> peliculas = new ListaEncadenada<VOPelicula>();
			for(int j = 0; j<listaPrincipalGeneros.get(i).darPeliculas().size(); j++){
				peliculas.agregarElementoFinal(convertirVOPelicula(listaPrincipalGeneros.get(i).darPeliculas().get(j)));
			}
			ILista<VOPelicula> peliculasOrdenadas = new ListaEncadenada<VOPelicula>();
			peliculasOrdenadas = quickSortAgnoPublicacion(peliculas, 0, peliculas.size()-1);
			generopelicula.setPeliculas(peliculasOrdenadas);
			retorno.agregarElementoFinal(generopelicula);	
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("opinionRatingsGeneroSR", inicio, end);
		return retorno;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		long inicio = Instant.now().getEpochSecond();

		ILista<VOPeliculaPelicula> retorno = new ListaEncadenada<VOPeliculaPelicula>();
		Cola<DatoPunto5A> datos = new Cola<DatoPunto5A>();
		BufferedReader br = null;
		try {
			String lineaActual = "";
			br = new BufferedReader(new FileReader(rutaRecomendacion));
			while ((lineaActual = br.readLine()) != null) {
				String[] values = lineaActual.split(",");
				int idPelicula = Integer.parseInt(values[0]);
				double rating = Double.parseDouble(values[1]);
				DatoPunto5A actual = new DatoPunto5A();
				actual.setCalificacion(rating);
				for(int i = 0; i<peliculas.size(); i++){
					if(peliculas.get(i).darId() == idPelicula){
						actual.setPelicula(convertirVOPelicula(peliculas.get(i)));
					}

				}
				datos.enqueue(actual);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		for(int a = 0; a<datos.size(); a++){

			ILista<Double> diferencias = new ListaEncadenada<Double>();
			DatoPunto5A datoActual = datos.dequeue();
			VOPeliculaPelicula vopeliculapelicula = new VOPeliculaPelicula();
			vopeliculapelicula.setPelicula(datoActual.darPelicula());
			ILista<VOPelicula> vopeliculas = new ListaEncadenada<VOPelicula>();

			for(int b = 0; b<peliculas.size(); b++){
				if(datoActual.darPelicula().getGenerosAsociados().get(0).equals(peliculas.get(b).darGeneros().get(0))){
					double diferencia = Math.abs(peliculas.get(b).darPromedioRating() - datoActual.darCalificacion());
					if(diferencia <= 0.5){
						diferencias.agregarElementoFinal(diferencia);

						VOPelicula pelicula = convertirVOPelicula(peliculas.get(b));
						vopeliculas.agregarElementoFinal(pelicula);
					}
				}
			}
			vopeliculapelicula.setPeliculasRelacionadas(quickSortCalificacionUsuarioDiferencia(vopeliculas, diferencias, 0, vopeliculas.size()-1));
			retorno.agregarElementoFinal(vopeliculapelicula);
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("recomendarPeliculasSR", inicio, end);
		return retorno;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		long inicio = Instant.now().getEpochSecond();

		ILista<VORating> retorno = new ListaEncadenada<VORating>();
		ILista<DatoPelicula> datos = new ListaEncadenada<DatoPelicula>();
		Pelicula movie = null;
		for(int i = 0; i<peliculas.size(); i++){
			if(peliculas.get(i).darId() == idPelicula){
				movie = peliculas.get(i);
			}
		}
		datos = movie.darRatings();
		ILista<DatoPelicula> datosOrdenado = quickSortRatingsPorFecha(datos, 0, datos.size()-1);
		ILista<VORating> lista = new ListaEncadenada<VORating>();
		for(int j = 0; j<datosOrdenado.size(); j++){
			VORating actual = new VORating();
			actual.setIdPelicula(datosOrdenado.get(j).darmovieID());
			actual.setIdUsuario(datosOrdenado.get(j).darUserID());
			actual.setRating(Double.parseDouble(datosOrdenado.get(j).darData()));
			retorno.agregarElementoFinal(actual);
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("ratingsPeliculaSR", inicio, end);

		return lista;
	}

	/**
	 * M�todo 1B
	 * @param n
	 * @return
	 */

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n)
	{
		long inicio = Instant.now().getEpochSecond();
		ILista<VOGeneroUsuario> resp = new ListaEncadenada<VOGeneroUsuario>();

		for(int i = 0; i < generoUsuario.size(); i++)
		{
			while(n!=0)
			{
				shellSortUsuariosMasActivos(generoUsuario.get(i).getUsuarios());
				resp.agregarElementoFinal(generoUsuario.get(i));
				n--;
			}
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("usuariosActivosSR", inicio, end);
		return resp;
	}

	/**
	 * M�todo de apoyo 1B
	 */

	public ILista<VOUsuarioConteo> shellSortUsuariosMasActivos(ILista<VOUsuarioConteo> pUsuarioConteo)
	{
		{
			int j;
			for( int gap = pUsuarioConteo.size()/ 2; gap > 0; gap /= 2 )
			{
				for( int i = gap; i < pUsuarioConteo.size(); i++ )
				{
					VOUsuarioConteo tmp = pUsuarioConteo.get(i);
					for( j = i; j >= gap && tmp.getConteo() <  pUsuarioConteo.get(j-gap).getConteo() ; j -= gap )
					{
						pUsuarioConteo.exchange(j, j-gap);
					}
					tmp = pUsuarioConteo.get(j);
				}
			}
		}
		return pUsuarioConteo;
	}


	/**
	 * M�todo que adicciona listas con listas
	 * @param resp
	 * @param usuarios
	 */
	public void addAll(ILista<VOUsuario> resp, ILista<VOUsuario> usuarios)
	{
		int cont = 0;
		while(usuarios.get(cont) != null)
		{
			resp.agregarElementoFinal(usuarios.get(cont));
			cont++;
		}
	}

	/**
	 * M�todo 2B
	 * @return
	 */

	public ILista<VOUsuario> darListaUsuariosPrimerTimeStamp ()
	{
		long inicio = Instant.now().getEpochSecond();
		ILista<VOUsuario> resp = new ListaEncadenada<VOUsuario>();
		for(int i = 0; i < listaPrincipalGeneros.size(); i++)
		{
			addAll(resp, listaPrincipalGeneros.get(i).darUsuarios());
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("darListaUsuariosPrimerTimeStamp", inicio, end);
		return insertionSortPrimerUsuarioEnCrearRating(resp);
	}

	/**
	 * M�todo 3B
	 */

	public ILista<VOPelicula> darPeliculasConMasTagsxGenero(String pGenero)
	{
		long inicio = Instant.now().getEpochSecond();
		ILista<VOPelicula> resp = new ListaEncadenada<VOPelicula>();

		for(int i = 0; i < listaPrincipalGeneros.size(); i++)
		{
			if(pGenero.equalsIgnoreCase((listaPrincipalGeneros.get(i).darNombre())))
			{
				resp = quickSortPeliculasConMasTags(listaPrincipalGeneros.get(i).darVOPeliculas(), 0, listaPrincipalGeneros.get(i).darVOPeliculas().size());

			}
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("darPeliculasCOnMasTagsxGenero", inicio, end);
		return resp;
	}

	/**
	 * M�todo 4B
	 */
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR(){
		long inicio = Instant.now().getEpochSecond();

		for(int i = 0; i < usuariosGenero.size(); i++ )
		{
			insertionSortGeneroTag(usuariosGenero.get(i).getListaGeneroTags());
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("opinionTagsGeneroSR", inicio, end);
		return usuariosGenero;
	}

	/**
	 * M�todo 5B
	 */

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n){
		long inicio = Instant.now().getEpochSecond();

		ILista<VOPeliculaUsuario> resp = new ListaEncadenada<VOPeliculaUsuario>();
		Cola<DatoPunto5B> datos = new Cola<DatoPunto5B>();

		BufferedReader br = null;

		try
		{
			String lineaActual = "";
			br = new BufferedReader(new FileReader(rutaRecomendacion));

			while((lineaActual = br.readLine()) != null)
			{
				String[] values = lineaActual.split(",");
				long idPelicula = Long.parseLong(values[0]);
				double rating = Double.parseDouble(values[1]);
				DatoPunto5B actual = new DatoPunto5B();
				actual.setRating(rating);
				actual.setIDPelicula(idPelicula);

				datos.enqueue(actual);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		double diferencia = 0.5;
		VOPeliculaUsuario PU = new VOPeliculaUsuario();
		for(int i = 0; i < datos.size(); i++)
		{
			DatoPunto5B data = datos.dequeue();

			ILista<VOUsuario> usuariospaPU = new ListaEncadenada<VOUsuario>();

			if(data.darIDPelicula() == voratings.get(i).getIdPelicula())
			{
				if(data.darRating() - voratings.get(i).getRating() <= diferencia)
				{
					PU.setIdPelicula(data.darIDPelicula());

					for(int j = 0; j < listaPrincipalGeneros.size(); j++)
					{
						while(voratings.get(i).getIdUsuario() == listaPrincipalGeneros.get(i).darUsuarios().get(j).getIdUsuario())
						{
							usuariospaPU.agregarElementoFinal(listaPrincipalGeneros.get(i).darUsuarios().get(j));
						}

					}
					PU.setUsuariosRecomendados(usuariospaPU);
				}
			}
			resp.agregarElementoFinal(PU);
		}
		long end = Instant.now().getEpochSecond();
		agregarOperacionSR("recomendarUsuariosSR", inicio, end);
		return resp;
	}

//////////////////////////////////// METODOS ADICIONALES /////////////////////////////////////////////////////

/**
 * M�todo 6B
 */
@Override
public ILista<VOTag> tagsPeliculaSR(int idPelicula) 
{
	return quickSortTagsOrdenadosxTiempo(extraerTagsDeVOPeliculas(Long.valueOf(idPelicula)), 0, extraerTagsDeVOPeliculas(Long.valueOf(idPelicula)).size());
}


@Override
public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
	// TODO Auto-generated method stub
	return null;
}




@Override
public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
	VOOperacion current = new VOOperacion();
	current.setOperacion(nomOperacion);
	current.setTimestampFin(tfin);
	current.setTimestampInicio(tinicio);
	pilaAcciones.push(current);
}

@Override
public ILista<VOOperacion> darHistoralOperacionesSR() {
	ILista<VOOperacion> lista = new ListaEncadenada<VOOperacion>();
	for(int i = 0; i<pilaAcciones.size(); i++){
		VOOperacion current = pilaAcciones.pop();
		lista.agregarElementoFinal(current);
	}
	for(int j = 0; j<lista.size(); j++){
		pilaAcciones.push(lista.get(j));
	}
	return lista;
}

@Override
public void limpiarHistorialOperacionesSR() {
	pilaAcciones = new Pila<VOOperacion>();

}

@Override
public ILista<VOOperacion> darUltimasOperaciones(int n) {
	if(n > pilaAcciones.size()-1){
		n = pilaAcciones.size() -1;
	}

	ILista<VOOperacion> lista = new ListaEncadenada<VOOperacion>();
	for(int i = 0; i<=n; i++){
		VOOperacion current = pilaAcciones.pop();
		lista.agregarElementoFinal(current);
	}
	for(int j = 0; j<lista.size(); j++){
		pilaAcciones.push(lista.get(j));
	}
	return lista;
}

@Override
public void borrarUltimasOperaciones(int n) {
	int operaciones = 0; 
	while(operaciones < n){
		pilaAcciones.pop();
		operaciones++;
	}
}

@Override
public long agregarPelicula(String titulo, int agno, String[] generos) {
	long mayor = 0;
	for(int j = 0; j<peliculas.size(); j++){
		if(peliculas.get(j).darId() > mayor){
			mayor = peliculas.get(j).darId();
		}
	}
	long id = mayor+1;
	Pelicula movie = new Pelicula(id, titulo, agno);
	for(int i = 0; i<generos.length; i++){
		movie.agregarGenero(generos[i]);
	}
	peliculas.agregarElementoFinal(movie);
	estructurarDatos();
	return id;
}

@Override
public void agregarRating(int idUsuario, int idPelicula, double rating) {
	DatoPelicula dato = new DatoPelicula(idUsuario, idPelicula, String.valueOf(rating), Instant.now().getEpochSecond());
	ratings.agregarElementoFinal(dato);
	estructurarDatos();

}

//////////////////////////////////// METODOS ADICIONALES /////////////////////////////////////////////////////

public VOPelicula convertirVOPelicula(Pelicula pPelicula){
	VOPelicula peli = new VOPelicula();
	peli.setAgnoPublicacion(pPelicula.darAgno());
	peli.setIdPelicula(pPelicula.darId());
	peli.setNumeroRatings(pPelicula.darRatings().size());
	peli.setNumeroTags(pPelicula.darTags().size());
	peli.setPromedioRatings(pPelicula.darPromedioRating());
	peli.setTitulo(pPelicula.darNombre());
	peli.setGenerosAsociados(pPelicula.darGeneros());

	return peli;
}

public void estructurarDatos(){

	listaPrincipalGeneros = new ListaEncadenada<Genero>();
	//borro los generos repetidos
	for(int i = 0; i<generos.size(); i++){
		for(int j = i+1; j<generos.size(); j++){
			if(generos.get(i).equals(generos.get(j))){
				generos.eliminarElemento(j);
			}
		}
	}
	//agrego las peliculas a sus generos correspondientes
	for(int i = 0; i<generos.size(); i++){
		Genero actual = new Genero();
		actual.setNombre(generos.get(i));
		ILista<Pelicula> peliculasEsteGenero = new ListaEncadenada<Pelicula>();
		for(int j = 0; j<peliculas.size(); j++){
			for(int k = 0; k<peliculas.get(j).darGeneros().size(); k++){
				if(peliculas.get(j).darGeneros().get(k).equals(generos.get(i))){
					peliculasEsteGenero.agregarElementoFinal(peliculas.get(j));
				}
			}
		}
		actual.setPeliculas(peliculasEsteGenero);
		listaPrincipalGeneros.agregarElementoFinal(actual);
	}


	//tags
	for(int i = 0; i<peliculas.size(); i++){
		for(int j = 0; j<tags.size(); j++){
			if(peliculas.get(i).darId() == tags.get(j).darmovieID()){
				peliculas.get(i).agregarTag(tags.get(j));
			}
		}
	}



	//ratings
	for(int i = 0; i<peliculas.size(); i++){
		for(int j = 0; j<ratings.size(); j++){
			if(peliculas.get(i).darId() == ratings.get(j).darmovieID()){
				peliculas.get(i).agregarRating(ratings.get(j));
			}
		}
	}
}
public ILista<VOPelicula> quickSortAgnoPublicacion(ILista<VOPelicula> pLista, int a, int b){

	int i = a;
	int j = b;
	int pivot = j/2;
	while(i <=j){
		while(pLista.get(i).getAgnoPublicacion() < pLista.get(pivot).getAgnoPublicacion()){
			i++;
		}
		while(pLista.get(j).getAgnoPublicacion() > pLista.get(pivot).getAgnoPublicacion()){
			j--;
		}
		if(i <= j){
			pLista.exchange(i,j);
			i++;
			j--;
		}
	}
	if(a <j){
		quickSortAgnoPublicacion(pLista, a,j);
	}
	if(i < b){
		quickSortAgnoPublicacion(pLista, i, b);
	}
	return pLista;
}



/**
 * METODO UTILIZADO PARA PUNTO 1A peliculasPopularesSR(int). 
 * @param pLista
 * @param a
 * @param b
 * @return ILista ordenada
 */
public ILista<Pelicula> quickSortRatingsPeliculas(ILista<Pelicula> pLista, int a, int b){
	int i = a;
	int j = b;
	int pivot = j/2;
	while(i <=j){
		while(pLista.get(i).darRatings().size() < pLista.get(pivot).darRatings().size()){
			i++;
		}
		while(pLista.get(j).darRatings().size() > pLista.get(pivot).darRatings().size()){
			j--;
		}
		if(i <= j){
			pLista.exchange(i,j);
			i++;
			j--;
		}
	}
	if(a <j){
		quickSortRatingsPeliculas(pLista, a,j);
	}
	if(i < b){
		quickSortRatingsPeliculas(pLista, i, b);
	}
	return pLista;
}

/**
 * METODO PARA EL PUNTO 6A (ratingsPeliculaSR(long))
 * @param pLista
 * @param a
 * @param b
 * @return lista VORating
 */
public ILista<DatoPelicula> quickSortRatingsPorFecha(ILista<DatoPelicula> pLista, int a, int b){
	int i = a;
	int j = b;
	int pivot = j/2;
	while(i <=j){
		while(pLista.get(i).darTimeStamp() < pLista.get(pivot).darTimeStamp()){
			i++;
		}
		while(pLista.get(j).darTimeStamp() > pLista.get(pivot).darTimeStamp()){
			j--;
		}
		if(i <= j){
			pLista.exchange(i,j);
			i++;
			j--;
		}
	}
	if(a <j){
		quickSortRatingsPorFecha(pLista, a,j);
	}
	if(i < b){
		quickSortRatingsPorFecha(pLista, i, b);
	}
	return pLista;
}
/**
 * METODO PARA PUNTO 5A (recomendarPeliculasSR)
 * @param pLista
 * @param pDiferencias
 * @param a
 * @param b
 * @return
 */
public ILista<VOPelicula> quickSortCalificacionUsuarioDiferencia(ILista<VOPelicula> pLista, ILista<Double> pDiferencias, int a, int b){
	int i = a;
	int j = b;
	int pivot = j/2;
	while(i <=j){
		while(pDiferencias.get(i) < pDiferencias.get(pivot)){
			i++;
		}
		while(pDiferencias.get(j) > pDiferencias.get(pivot)){
			j--;
		}
		if(i <= j){
			pLista.exchange(i,j);
			i++;
			j--;
		}
	}
	if(a <j){
		quickSortCalificacionUsuarioDiferencia(pLista, pDiferencias, a,j);
	}
	if(i < b){
		quickSortCalificacionUsuarioDiferencia(pLista, pDiferencias, i,b);
	}
	return pLista;
}

/**
 * METODO PARA PUNTO 5A (recomendarPeliculasSR)
 * @param pLista
 * @param pDiferencias
 * @param a
 * @param b
 * @return
 */

/**
 * M�todo xxxxx 2B
 */

public ILista<VOUsuario> quickSortPrimerUsuarioEnCrearRating(ILista<VOUsuario> pLista, int a, int b)
{
	int i = a;
	int j = b;
	int pivot = j/2;
	while(i <=j){
		while(pLista.get(i).getPrimerTimestamp() < pLista.get(pivot).getPrimerTimestamp()){
			i++;
		}
		while(pLista.get(j).getPrimerTimestamp()> pLista.get(pivot).getPrimerTimestamp()){
			j--;
		}
		if(i <= j){
			pLista.exchange(i,j);
			i++;
			j--;
		}
	}
	if(a <j){
		quickSortPrimerUsuarioEnCrearRating(pLista, a,j);
	}
	if(i < b){
		quickSortPrimerUsuarioEnCrearRating(pLista, i, b);
	}
	return pLista;
}

/**
 * M�todo para el punto 2B
 */
public ILista<VOUsuario> insertionSortPrimerUsuarioEnCrearRating(ILista<VOUsuario> pLista)
{
	for(int i = 0; i < pLista.size(); i++)
	{
		for(int j = i + 1; j < pLista.size() - 1; j++)
		{
			VOUsuario valorI = pLista.get(i);
			VOUsuario valorJ = pLista.get(j);
			if( valorI.getPrimerTimestamp() > valorJ.getPrimerTimestamp())
			{
				pLista.exchange(i, j);
			}
			else if(valorI.getPrimerTimestamp()<valorJ.getPrimerTimestamp())
			{
			}

			else
			{
				if(valorI.getNumRatings() > valorJ.getNumRatings())
				{
					pLista.exchange(i, j);
				}
				else if(valorI.getNumRatings() < valorJ.getNumRatings())
				{

				}
				else
				{
					if(valorI.getIdUsuario() > valorJ.getIdUsuario())
					{
						pLista.exchange(i, j);
					}
					else
					{

					}
				}
			}
		}

	}
	return pLista;
}

public ILista<VOPelicula> quickSortPeliculasConMasTags(ILista<VOPelicula> pPeliculas, int a, int b)
{
	int i = a;
	int j = b;
	int pivot = j/2;
	while(i <=j){
		while(pPeliculas.get(i).getNumeroTags() < pPeliculas.get(pivot).getNumeroTags()){
			i++;
		}
		while(pPeliculas.get(j).getNumeroTags()> pPeliculas.get(pivot).getNumeroTags()){
			j--;
		}
		if(i <= j){
			pPeliculas.exchange(i,j);
			i++;
			j--;
		}

	}
	if(a <j)
	{
		quickSortPeliculasConMasTags(pPeliculas, a,j);
	}
	if(i < b)
	{
		quickSortPeliculasConMasTags(pPeliculas, i, b);
	}
	return pPeliculas;
}
/**
 * M�todo de apoyo 6B
 */

public ILista<VOTag> extraerTagsDeVOPeliculas(long pIDPelicula)
{
	ILista<String> tagsDPeli = new ListaEncadenada<String>();
	ILista<VOTag> resp = new ListaEncadenada<VOTag>();
	for(int i = 0; i < listaPrincipalGeneros.size(); i++)
	{

		for(int j = 0; j < listaPrincipalGeneros.get(i).darVOPeliculas().size(); j++)
		{
			if(listaPrincipalGeneros.get(i).darVOPeliculas().get(j).getIdPelicula() == pIDPelicula)
				for(int k = 0; k < listaPrincipalGeneros.get(j).darVOPeliculas().get(j).getTagsAsociados().size(); k++)
				{
					tagsDPeli.agregarElementoFinal(listaPrincipalGeneros.get(i).darVOPeliculas().get(j).getTagsAsociados().get(k));
				}
		}

	}

	for(int l = 0; l < listaPrincipalGeneros.size(); l++)
	{
		for(int m = 0; m < listaPrincipalGeneros.get(l).darTags().size(); m++)
		{
			if(listaPrincipalGeneros.get(l).darTags().get(m).getTag().equalsIgnoreCase(tagsDPeli.get(l)))
			{
				resp.agregarElementoFinal(listaPrincipalGeneros.get(l).darTags().get(m));
			}

		}

	}
	return resp;
}

public ILista<VOTag> quickSortTagsOrdenadosxTiempo(ILista<VOTag> pTags, int a, int b)
{
	int i = a;
	int j = b;
	int pivot = j/2;
	while(i <=j){
		while(pTags.get(i).getTimestamp() < pTags.get(pivot).getTimestamp()){
			i++;
		}
		while(pTags.get(j).getTimestamp()> pTags.get(pivot).getTimestamp()){
			j--;
		}
		if(i <= j){
			pTags.exchange(i,j);
			i++;
			j--;
		}
	}
	if(a <j)
	{
		quickSortTagsOrdenadosxTiempo(pTags, a,j);
	}
	if(i < b)
	{
		quickSortTagsOrdenadosxTiempo(pTags, i, b);
	}
	return pTags;
}

/**
 * Insertion sort de 4B
 */

public ILista<VOGeneroTag> insertionSortGeneroTag(ILista<VOGeneroTag> generoTag)
{

	for(int i = 0; i < generoTag.size(); i++)
	{
		for(int j = i + 1; j < generoTag.size() - 1; j++)
		{
			String valorI = generoTag.get(i).getTags().get(j);
			String valorJ = generoTag.get(j).getTags().get(j);
			if( valorI.compareToIgnoreCase(valorJ) > 0)
			{
				generoTag.exchange(i, j);
			}
			else if( valorI.compareToIgnoreCase(valorJ) < 0)
			{
			}

			else
			{
			}
		}

	}
	return generoTag;
}

public ILista<Pelicula> mergesort(ILista<Pelicula> a, int low, int high) 
{
	int N = high - low;         
	if (N <= 1) 
		return a; 
	int mid = low + N/2; 
	a = mergesort(a, low, mid); 
	a = mergesort(a, mid, high); 
	Pelicula[] temp = new Pelicula[N];
	int i = low, j = mid;
	for (int k = 0; k < N; k++) 
	{
		if (i == mid)  
			temp[k] = a.get(j++);
		else if (j == high) 
			temp[k] = a.get(i++);
		else if (a.get(j).darNombre().compareTo(a.get(j).darNombre()) < 0) 
			temp[k] = a.get(j++);
		else 
			temp[k] = a.get(i++);
	}    
	for (int k = 0; k < N; k++) {
		a.set(low+k, temp[k]);       
	}

	high = a.size()-1;
	low = 0;
	N = a.size()-1;        
	if (N <= 1) 
		return a; 
	mid = low + N/2; 
	a = mergesort(a, low, mid); 
	a = mergesort(a, mid, high); 
	temp = new Pelicula[N];
	i = low;
	j = mid;
	for (int k = 0; k < N; k++) 
	{
		if (i == mid)  
			temp[k] = a.get(j++);
		else if (j == high) 
			temp[k] = a.get(i++);
		else if (a.get(j).darPromedioRating()<a.get(i).darPromedioRating())
			temp[k] = a.get(j++);
		else 
			temp[k] = a.get(i++);
	}    
	for (int k = 0; k < N; k++) {
		a.set(low+k, temp[k]);       
	}

	//agno
	high = a.size()-1;
	low = 0;
	N = high - low;         
	if (N <= 1) 
		return a; 
	mid = low + N/2; 
	a = mergesort(a, low, mid); 
	a =  mergesort(a, mid, high); 
	temp = new Pelicula[N];
	i = low;
	j = mid;
	for (int k = 0; k < N; k++) 
	{
		if (i == mid)  
			temp[k] = a.get(j++);
		else if (j == high) 
			temp[k] = a.get(i++);
		else if (a.get(j).darAgno()<a.get(i).darAgno()) 
			temp[k] = a.get(j++);
		else 
			temp[k] = a.get(i++);
	}    
	for (int k = 0; k < N; k++) {
		a.set(low+k, temp[k]);       
	}
	return a;
}
}

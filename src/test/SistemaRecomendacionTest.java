package test;

import api.SistemaRecomendacion;
import junit.framework.TestCase;
import model.data_structures.ILista;
import model.logic.DatoPelicula;
import model.logic.Pelicula;

public class SistemaRecomendacionTest extends TestCase {


	private SistemaRecomendacion sistema;

	public void setup(){
		sistema = new SistemaRecomendacion();
	}

	public void testCargarPeliculasSR(){
		String linea = "24, Powder (1995), Drama|Sci-Fi";
		int cuantasComas = 0;
		for(int  i = 0; i<linea.length(); i++){
			if(linea.charAt(i) == ','){
				cuantasComas++;	
			}
		}

		int id = Integer.parseInt(linea.split(",")[0].trim());
		String[] textoSeparadoComa = linea.split(",");
		String textoEntreIDyGeneros = "";
		for(int k = 1; k<cuantasComas; k++){
			textoEntreIDyGeneros = textoEntreIDyGeneros.trim() + " " + textoSeparadoComa[k].trim();;
		}

		int cuantosParentesis = 0;
		for(int h = 0; h<textoEntreIDyGeneros.length(); h++){
			if(textoEntreIDyGeneros.charAt(h) == '('){
				cuantosParentesis++;
			}
		}

		int agno = Integer.parseInt(textoEntreIDyGeneros.split("\\(")[cuantosParentesis].split("\\)")[0]);
		String[] textoNombre = textoEntreIDyGeneros.split("\\(");
		String nombre = "";
		for(int a = 0; a<cuantosParentesis; a++){
			nombre = nombre + textoNombre[a].trim();
		}
		String[] generosActual = linea.split(",")[cuantasComas].split("\\|");

		System.out.println(nombre);
		System.out.println(agno);
		System.out.println(id);
		for(int i = 0; i<generosActual.length; i++){
			System.out.println(generosActual[i].trim());
		}


		assertTrue("Se cargo sin errores", true);
	}

}
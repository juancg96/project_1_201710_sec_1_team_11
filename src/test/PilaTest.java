package test;
import junit.framework.TestCase;
import model.data_structures.Pila;

public class PilaTest extends TestCase{

	private Pila<Integer> pila;
	
	public void setup1(){
		pila = new Pila<Integer>();
		pila.push(43);
		pila.push(22);
		pila.push(76);
		pila.push(35);
		pila.push(81);
	}
	
	public void setup2(){
		pila = new Pila<Integer>();
	}
	
	public void testSize(){
		setup1();
		assertEquals("No se calculo correctamente el tamaño", 5, pila.size());
	}
	public void testIsEmpty(){
		setup1();
		assertFalse("Error al determinar si la pila estaba vacia", pila.isEmpty());
		setup2();
		assertTrue("Error al determinar si la pila estaba vacia", pila.isEmpty());
	}
	public void testPush(){
		setup1();
		pila.push(55);
		boolean funciono = false;
		if(pila.darTope() == 55){
			funciono = true;
		}
		assertTrue("Error al hacer push", funciono);
	}
	public void testPop(){
		setup1();
		Integer data = pila.pop();
		boolean funciono = false;
		if(data == 81 && pila.size() == 4 && pila.darTope() == 35){
			funciono = true;
		}
		assertTrue("Error al hacer pop", funciono);
	}
}

package test;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest<T> extends TestCase{

	private ListaEncadenada<Integer> lista;

	public void setup1(){
		lista = new ListaEncadenada<Integer>();
		lista.agregarElementoFinal(23);
		lista.agregarElementoFinal(35);
		lista.agregarElementoFinal(44);	
	}
	
	public void setup2(){
		lista = new ListaEncadenada<Integer>();
	}
	
	public void setup3(){
		lista = new ListaEncadenada<Integer>();
		lista.agregarElementoFinal(00);
		lista.agregarElementoFinal(11);
		lista.agregarElementoFinal(22); //2	
		lista.agregarElementoFinal(33);	
		lista.agregarElementoFinal(44);	
		lista.agregarElementoFinal(55);	// 5
		lista.agregarElementoFinal(66);	
		lista.agregarElementoFinal(77);	
		lista.agregarElementoFinal(88);	//8
		lista.agregarElementoFinal(99);		
		
	}
	
	public void testDarNumeroElementos(){
		System.out.println("test1");
		setup1();
		assertEquals("No se calculo el numero correcto de elementos", 3, lista.size());
	}

	public void testDarElemento(){
		System.out.println("test2");
		setup1();
		boolean funciono = false;
		Integer dato1 = lista.get(0);
		Integer dato2 = lista.get(1);
		Integer dato3 = lista.get(2);

		if(dato1 == 23 && dato2 == 35 && dato3 == 44) {
			funciono = true;
		}
		assertTrue("No devolvio el dato que era", funciono);
	}
	
	public void testEliminarElemento(){
		System.out.println("test3");
		setup1();
		Integer eliminado = lista.eliminarElemento(2);
		boolean funciono = false;
		if(eliminado == 44 && lista.size() == 2 && lista.get(1) == 35){
			funciono = true;
		}
		assertTrue("No se elimino correctamente", funciono);
	}
	
	public void testAgregarElementoFinal(){
		System.out.println("test4");
		setup2();
		lista.agregarElementoFinal(55);
		lista.agregarElementoFinal(98);
		boolean funciono = false;
		if(lista.get(0) == 55 && lista.get(1) == 98){
			funciono = true;
		}
		assertTrue("No se agrego correctamente", funciono);
		
	}
	
	public void testExchange(){
		setup3();
		lista.exchange(2,9);
		for(int i = 0; i<lista.size(); i++){
			System.out.println(lista.get(i));
		}
		boolean funciono = false;
		if(lista.get(2) == 99 && lista.get(9) == 22){
			funciono = true;
		}
		assertTrue("funciono el cambio", funciono);
	}
}

package test;

import junit.framework.TestCase;
import model.data_structures.Cola;

public class ColaTest extends TestCase {

	private Cola<Integer> cola;

	public void setup(){
		cola = new Cola<Integer>();

		cola.enqueue(24);
		cola.enqueue(33);
		cola.enqueue(87);
		cola.enqueue(65);
	}

	public void testDequeue(){
		setup();
		Integer borrado = cola.dequeue();
		boolean funciono = false;
		if(borrado == 24){
			funciono = true;
		}
		assertTrue("No se hizo dequeue correctamente", funciono);
	}

	public void testEnqueue(){
		cola = new Cola<Integer>();
		cola.enqueue(34);
		boolean funciono = false;
		if(cola.dequeue() == 34){
			funciono = true;
		}
		assertTrue("No se hizo enqueue correctamente", funciono);
	}
	
	public void testSize(){
		setup();
		assertEquals("No se calculo bien el tamaño", 4, cola.size());
	}
	
	public void testIsEmpty(){
		setup();
		assertFalse("No se determino si esta vacio correctamente", cola.isEmpty());
	}
}